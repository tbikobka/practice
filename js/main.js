var app = new Vue({
    el: "#prod",
    data:
    {
        products:
        [
            {
                id: 1,
                title: 'Artichoke',
                short_text: 'art',
                image: 'Artichoke.jpg',
                desc:'Description'
            },

            {
                id: 2,
                title:  'Silverskin',
                short_text: 'silv',
                image: 'Silverskin.jpg',
                desc:'Description' 
            },

            {
                id: 3,
                title: 'Turban',
                short_text: 'tur',
                image: 'Turban.jpg',
                desc:'Description' 
            },

            {
                id: 4,
                title: 'Creole',
                short_text: 'creo',
                image: 'Creole.jpg',
                desc:'Description' 
            },

            {
                id: 5,
                title: 'Asiatic',
                short_text: 'asic',
                image: 'Asiatic.jpg',
                desc:'Description' 
            }
        ],

        product: [],
        cart: [],
        contactFields: {},
        btnVisible: 0
    },

    mounted: function()
    {
        this.getProduct();
        this.checkInCart();
        this.getCart();
    },

    methods:
    {
        getProduct: function()
        {
            if(window.location.hash)
            {
                var id = window.location.hash.replace('#', '');
                if(this.products && this.products.length > 0)
                {
                    for(i in this.products)
                    {
                        if(this.products[i] && this.products[i].id && id==this.products[i].id)
                            this.product = this.products[i];
                    }
                }
            }
        },

        addToCart: function(id)
        {
            var cart = [];
            if(window.localStorage.getItem('cart'))
            {
                cart = window.localStorage.getItem('cart').split(',');
            }

            if(cart.indexOf(String(id)) == -1)
            {
                cart.push(id);
                window.localStorage.setItem('cart', cart.join());
                this.btnVisible=1;
            }

        },

        checkInCart: function()
        {
            if(this.products && this.product.id && window.localStorage.getItem('cart').split(',').indexOf(String(this.product.id)) != -1) this.btnVisible=1;
        },

        getCart: function()
        {
            var tcart = window.localStorage.getItem('cart').split(',');
            for(i in tcart)
            {
                for(j in this.products)
                {
                    if(tcart[i] == this.products[j].id)
                    {
                        this.cart.push(this.products[j]);
                    }
                }
            }
        },

        removeFromCart: function(id)
        {
            var c = window.localStorage.getItem('cart').split(',');
            c.splice(c.indexOf(String(id)), 1);
            window.localStorage.setItem('cart', c.join());

            for(i in this.cart)
            {
                if(this.cart[i].id == id)
                {
                    this.cart.splice(i, 1);
                }
            }
        },

        makeOrder: function()
        {
            document.getElementById('form').setAttribute('style', 'display:none');
            document.getElementById('form_info').setAttribute('style', 'display:block');

            localStorage.clear();
            this.cart.splice(0);
        }
    }
});