let bgsubwindow = document.getElementById('bg-sub-window');
let subwindow = document.getElementById('sub-window');
let subbtn = document.getElementById('sub-btn');
let subclose = document.getElementById('sub-close');
let subyes = document.getElementById('sub-yes');
let subno = document.getElementById('sub-no');

if('serviceWorker' in navigator)
{
    navigator.serviceWorker
    .register('/sw.js')
    .then(function()
    {
        console.log('Service worker registered!');
    });

    navigator.serviceWorker.ready.then(registration =>
    {
        if('PushManager' in window)
        {
            subyes
                .addEventListener('click', () =>
                {
                    console.log(1);
                    askPermission();
                });
        }
    });
}

subbtn.addEventListener('click', () =>
{
    bgsubwindow.style.display = 'block';
    subwindow.style.display = 'block';
});

subclose.addEventListener('click', hideSubWindow);

bgsubwindow.addEventListener('click', () =>
{
    if(!subwindow.contains(event.target))
    {
        hideSubWindow();
    }
});

subyes.addEventListener('click', askPermission);

subno.addEventListener('click', hideSubWindow);

function askPermission()
{
    return new Promise((resolve, reject) =>
    {
        const permissionResult = Notification.requestPermission(result =>
        {
            resolve(result);
        });
        
        if(permissionResult)
        {
            permissionResult.then(resolve, reject);
        }
    })
    .then(permissionResult =>
    {
        if(permissionResult !== 'granted')
        {
            throw new Error('We weren\'t granted permission.');
        }
    });
}

function hideSubWindow()
{
    bgsubwindow.style.display = 'none';
    subwindow.style.display = 'none';
}